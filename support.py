#!/usr/bin/env python3

import os, sys, datetime, subprocess
from os.path import expanduser
from collections import namedtuple
from xml.dom import minidom

def info(s):
    print('\n\x1b[1;37;44mINFO\x1b[0m %s' % s)

def error(s):
    print('\n\x1b[1;37;41mERROR\x1b[0m %s' % s)

# set basedir to <workspace-root>:
basedir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

now = datetime.datetime.now().strftime('%Y%m%dT%H%M%S')

def run(cmd, cwd=None, ignoreError=False):
    if not cwd:
        cwd = basedir
    elif not cwd.startswith('/'):
        cwd = os.path.normpath(basedir + '/' + cwd)
    print('\n\x1b[6;30;47m%s\x1b[0m $ %s' % (cwd, cmd))
    code = subprocess.call(cmd, cwd=cwd, shell=True)
    if code != 0:
        error('command failed: %s' % cmd)
        if not ignoreError:
            exit(1)

def fileExist(p):
    if not p.startswith('/'):
        return os.path.isfile(basedir + '/' + p)
    return os.path.isfile(p)

def dirExist(p):
    if not p.startswith('/'):
        return os.path.isdir(basedir + '/' + p)
    return os.path.isdir(p)

def moduleExist(module):
    fs = os.path.join(basedir, 'crypto-build', module)
    return os.path.isdir(os.path.normpath(fs))

def parsePOM():
    doc = minidom.parse(os.path.join(basedir, 'crypto-build', 'pom.xml'))
    root = doc.documentElement
    modules = root.getElementsByTagName('modules')[0]
    removed = []
    exists = []
    for node in modules.childNodes:
        if node.nodeType == 1:
            # this is an element
            fp = node.childNodes[0].nodeValue
            if moduleExist(fp):
                info('module %s exist.' % fp)
                exists.append(App(dir=fp[fp.find('/') + 1 : fp.rfind('/')], name=fp[fp.rfind('/') + 1 :]))
            else:
                error('module %s is not exist. ignored.' % fp)
                removed.append(node)
    for node in removed:
        modules.removeChild(node)
    return exists, doc.toxml()

def appexist(app):
    return os.path.isdir(os.path.join(basedir, app.dir)) and os.path.isdir(os.path.join(basedir, app.dir, app.name))

App = namedtuple('App', ('dir', 'name'))

apps, xml = parsePOM()

with open(os.path.join(basedir, 'crypto-build', 'pom.xml.tmp'), 'w') as tmp:
    tmp.write(xml)
