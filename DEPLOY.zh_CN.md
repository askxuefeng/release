# Demo部署流程

Demo环境可将所有组件部署至一台32G内存、20G硬盘的机器。

## 创建Ubuntu Server

首先创建EC2，选择`Ubuntu Server 18.04 LTS (HVM), SSD Volume Type`，64bit x86。

公网IP：必须分配公网IP

安全组设置：

- 输入
  - 允许TCP 22: 0.0.0.0/32
  - 允许TCP 80: 0.0.0.0/32
- 输出
  - 允许所有

用户名：`ubuntu`，具有无密码`sudo`权限。

更新Ubuntu并重启：

```
$ sudo apt update
$ sudo apt upgrade
$ sudo reboot
```

#### 安装OpenJDK 11

使用`apt`安装OpenJDK 11：

```
$ sudo apt install openjdk-11-jre-headless
```

#### 安装Nginx

使用`apt`安装Nginx：

```
$ sudo apt install nginx
```

#### 安装MySQL 5.7

使用`apt`安装MySQL 5.7：

```
$ sudo apt install mysql-server-5.7
```

然后修改`root`登录密码为`password`。

首先查看默认用户`debian-sys-maint`安装密码：

```
$ sudo more /etc/mysql/debian.cnf
```

然后以`debian-sys-maint`登录并修改`root`密码为`password`：

```
$ mysql -u debian-sys-maint -p
Enter password: ******** (password of debian-sys-maint can be get from sudo cat /etc/mysql/debian.cnf)
mysql> UPDATE mysql.user SET authentication_string=PASSWORD('password'), plugin='mysql_native_password' where user='root';
Query OK, 1 row affected, 1 warning (0.00 sec)
Rows matched: 1  Changed: 1  Warnings: 1
mysql> exit
```

修改默认配置`/etc/mysql/mysql.conf.d/mysqld.cnf`并设置最大连接数为`2000`：

找到`#max_connections = 100`去掉注释并设置为`2000`或更高。

重启MySQL并检查：

```
$ sudo service mysql restart
$ mysql -u root -p
Enter password: ********
mysql> show variables like '%max_connection%';
+--------------------+-------+
| Variable_name      | Value |
+--------------------+-------+
| max_connections    | 2001  |
+--------------------+-------+
```

#### 安装Redis 5.x/6.x

通过`apt`源安装Redis 5.x/6.x：

```
$ sudo add-apt-repository ppa:chris-lea/redis-server
$ sudo apt install redis-server
$ redis-cli --version
redis-cli 6.0.x
```

#### 安装Kafka

下载并解压Kafka：

```
$ cd ~
$ wget https://downloads.apache.org/kafka/2.5.0/kafka_2.12-2.5.0.tgz
$ tar zxf kafka_2.12-2.5.0.tgz
$ ln -s kafka_2.12-2.5.0 kafka
```

在目录`~`创建脚本`nohup-start-zookeeper.sh`和`nohup-start-kafka`以便快速启动Kafka：

文件`nohup-start-zookeeper.sh`：

```
#!/bin/bash
cd "$(dirname "$0")"
cd kafka
echo "bin/zookeeper-server-start.sh config/zookeeper.properties"
nohup bin/zookeeper-server-start.sh config/zookeeper.properties > nohup-zookeeper.log 2>&1 &
```

文件`nohup-start-kafka`：

```
#!/bin/bash
cd "$(dirname "$0")"
cd kafka
echo "bin/kafka-server-start.sh config/server.properties"
nohup bin/kafka-server-start.sh config/server.properties > nohup-kafka.log 2>&1 &
```

设置为可执行：

```
$ chmod a+x nohup-start-*.sh
```

## 部署

下载release至本地目录。

将release所有文件上传至`/home/ubuntu/deploy`目录：

服务器执行：

```
$ mkdir ~/deploy
```

本地执行：

```
$ scp * ubuntu@<ip-address>:/home/ubuntu/deploy
```

在服务器解压所有`.tar.gz`文件：

```
$ pwd
/home/ubuntu/deploy
$ sh unzip-all.sh
```

### 配置Nginx

将配置文件链接至：

```
$ cd /etc/nginx/sites-enabled
$ sudo rm default
$ sudo ln -s /home/ubuntu/deploy/nginx-proxy/config/default.conf default
$ sudo service nginx reload
```

### 初始化数据库

解压`sql.zip`，然后执行：

```
$ pwd
/home/ubuntu/deploy
$ sh clean-db.sh
```

用`root`登录后可见数据库：

```
$ mysql -u root -p
Enter password: ********
Welcome to the MySQL monitor...

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| crypto_contracts   |
| crypto_hd          |
| crypto_manage      |
| crypto_options     |
| crypto_quotation   |
| crypto_shared      |
| crypto_spots       |
| crypto_ui          |
| crypto_wallet      |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
13 rows in set (0.00 sec)
```

### 创建存储目录

本地运行时交易引擎将状态存储至本地文件，创建目录如下：

```
$ sudo mkdir /data
$ sudo mkdir /data/spots-snapshot
$ sudo mkdir /data/contracts-snapshot
$ sudo chown ubuntu:ubuntu /data/*
```

### 启动Exchange

在`~/deploy`目录下执行：

```
$ su start-all.sh
```

成功启动后，使用`ps`可见如下进程：

```
$ ps aux | grep crypto
ubuntu    7342  1.3  1.9 8120384 1243848 ?     Sl   Jun28  58:35 java -server -Xmx2048M -jar /home/ubuntu/deploy/contracts-api/crypto-contracts-api.jar
ubuntu    7356  0.9  1.6 8043468 1063132 ?     Sl   Jun28  40:44 java -server -Xmx2048M -jar /home/ubuntu/deploy/contracts-sequence/crypto-contracts-sequence.jar
ubuntu    7380  1.2  1.6 8053740 1084112 ?     Sl   Jun28  56:11 java -server -Xmx2048M -jar /home/ubuntu/deploy/contracts-store/crypto-contracts-store.jar
ubuntu    7427  0.7  1.7 8048616 1122956 ?     Sl   Jun28  32:50 java -server -Xmx2048M -jar /home/ubuntu/deploy/manage/crypto-manage.jar
ubuntu    7457  0.8  1.5 8040380 1015400 ?     Sl   Jun28  36:52 java -server -Xmx2048M -jar /home/ubuntu/deploy/options-sequence/crypto-options-sequence.jar
ubuntu    7478  0.6  1.6 7131876 1055480 ?     Sl   Jun28  28:58 java -server -Xmx2048M -jar /home/ubuntu/deploy/push/crypto-push.jar
ubuntu    7508  1.5  1.6 8068964 1079488 ?     Sl   Jun28  68:11 java -server -Xmx2048M -jar /home/ubuntu/deploy/quotation/crypto-quotation.jar
ubuntu    7545  1.1  1.9 8106068 1274768 ?     Sl   Jun28  48:46 java -server -Xmx2048M -jar /home/ubuntu/deploy/shared-api/crypto-shared-api.jar
ubuntu    7574  1.2  1.8 8096640 1229996 ?     Sl   Jun28  56:41 java -server -Xmx2048M -jar /home/ubuntu/deploy/spots-api/crypto-spots-api.jar
ubuntu    7603  0.9  1.6 8045908 1094648 ?     Sl   Jun28  41:05 java -server -Xmx2048M -jar /home/ubuntu/deploy/spots-sequence/crypto-spots-sequence.jar
ubuntu    7636  1.2  1.6 8051548 1076872 ?     Sl   Jun28  53:41 java -server -Xmx2048M -jar /home/ubuntu/deploy/spots-store/crypto-spots-store.jar
ubuntu    7684  0.7  1.8 8045500 1188432 ?     Sl   Jun28  31:52 java -server -Xmx2048M -jar /home/ubuntu/deploy/ui/crypto-ui.jar
ubuntu    7710  0.7  1.6 8076228 1082272 ?     Sl   Jun28  33:28 java -server -Xmx2048M -jar /home/ubuntu/deploy/wallet-api/crypto-wallet-api.jar
ubuntu    9376  1.1  1.7 8068348 1152704 ?     Sl   Jun28  48:26 java -server -Xmx2048M -jar /home/ubuntu/deploy/spots-trading/crypto-spots-trading.jar
ubuntu    9436  1.1  1.7 8058896 1124720 ?     Sl   Jun28  51:12 java -server -Xmx2048M -jar /home/ubuntu/deploy/contracts-trading/crypto-contracts-trading.jar
ubuntu   16719  0.0  0.0  14852  1088 pts/0    S+   12:59   0:00 grep --color=auto crypto
```

日志文件存储在`~/deploy/<app>/logs`。

### 映射域名

将以下域名映射添加至本地Hosts文件：

```
<ubuntu-ip-address> www.demo.interplanetbit.com wss.demo.interplanetbit.com api.demo.interplanetbit.com static.demo.interplanetbit.com  manage.demo.interplanetbit.com
```

# 访问交易系统

用户通过`www.demo.interplanetbit.com`登录，数据库初始化脚本已创建如下用户：

```
email            password
-------------------------
bot0@example.com password
bot1@example.com password
bot2@example.com password
bot3@example.com password
bot4@example.com password
bot5@example.com password
bot6@example.com password
bot7@example.com password
bot8@example.com password
bot9@example.com password
```

管理员通过`manage.demo.interplanetbit.com`登录管理后台。
