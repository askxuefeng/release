#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, json, time, random, datetime, threading

from urllib import request, parse

# set base dir:
basedir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
print('base dir is set to: %s' % basedir)

from sdk import ApiClient

SYMBOL = 'XBTC'
API_HOST = 'api.sandbox.interplanetbit.com'

# fetch price from coinbase:
SYMBOLS_MAPPING = {
    'BTC_USDT': 'BTC-USD',
    'XBTC': 'BTC-USD'
}

SLEEP = 3

def main():
    num = 1
    host = API_HOST
    symbol = SYMBOL
    https = True
    debug = True
    for arg in sys.argv:
        if arg.startswith('--host='):
            host = arg[7:]
        if arg.startswith('--https='):
            https = arg[8:].lower() == 'true'
        if arg.startswith('--num='):
            num = int(arg[6:])
        if arg.startswith('--symbol='):
            symbol = arg[9:]
        if arg.startswith('--debug='):
            debug = arg[8:].lower() == 'true'
    remoteSymbol = SYMBOLS_MAPPING.get(symbol)
    if remoteSymbol is None:
        log('ERROR: invalid symbol: %s' % symbol)
        exit(1)
    keys = load_keys(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'init-keys.json'))
    for i in range(num):
        kp = keys[i]
        t = threading.Thread(target=lambda: do_jobs(kp['apiKey'], kp['apiSecret'], host, https, symbol, remoteSymbol, debug))
        t.start()

def load_keys(filename):
    with open(filename, 'r') as fp:
        return json.load(fp)

def do_jobs(key, secret, host, https, symbol, remoteSymbol, debug):
    log('start bot for %s, key = %s, secret = %s******...' % (symbol, key, secret[0:6]))
    client = ApiClient(key, secret, host, https, debug=debug)
    while True:
        try:
            world_price = get_world_price(remoteSymbol)
            log('world price: %s' % world_price)
            exist_orders = cancel_order(client, symbol)
            if exist_orders < 20:
                if random.random() < 0.5:
                    place_limit_buy_order(client, symbol, world_price)
                    place_limit_sell_order(client, symbol, world_price)
                else:
                    place_limit_sell_order(client, symbol, world_price)
                    place_limit_buy_order(client, symbol, world_price)
            place_market_order(client, symbol, world_price)
        except Exception as e:
            log(e)
        time.sleep(random.random() * SLEEP)

def cancellable_order(o):
    if o.type == 'LIMIT':
        if o.status == 'PENDING' or o.status == 'PARTIAL_FILLED':
            return True
    return False

def cancel_order(client, symbol):
    log('get orders...')
    r = client.get('/v1/contracts/orders/open', symbol=symbol)
    if success(r):
        orders = list(filter(cancellable_order, r.results))
        log('%s orders can be cancelled.' % len(orders))
        if len(orders) > 18:
            targetOrder = orders[-1]
            log('cancel order %s %s' % (targetOrder.type, targetOrder.id))
            r = client.post('/v1/contracts/orders/%s/cancel' % targetOrder.id)
            log('cancelled ok? %s' % error(r))
        return len(orders) - 1
    return 0

def success(result):
    return not hasattr(result, 'error')

def error(result):
    if hasattr(result, 'error'):
        return 'ERROR %s: %s' % (result.error, result.message)
    return 'OK'

def place_market_order(client, symbol, world_price):
    amount = int(random.random() * 100 + 1)
    r = client.post('/v1/contracts/orders', {
        'symbol': symbol,
        'direction': 'SHORT',
        'type': 'MARKET',
        'quantity': amount
    })
    r = client.post('/v1/contracts/orders', {
        'symbol': symbol,
        'direction': 'LONG',
        'type': 'MARKET',
        'quantity': amount
    })

def place_limit_buy_order(client, symbol, world_price):
    price = world_price
    delta = price / 1000.0
    for i in range(3):
        price = int(price - rndbuy() * delta)
        quantity = int(random.random() * 100 + 10)
        r = client.post('/v1/contracts/orders', {
            'symbol': symbol,
            'type': 'LIMIT',
            'direction': 'LONG',
            'leverage': 10,
            'price': price,
            'quantity': quantity
        })
        log('place BUY_LIMIT at %s ok? %s' % (price, error(r)))
        time.sleep(random.random() * SLEEP)

def place_limit_sell_order(client, symbol, world_price):
    price = world_price
    delta = price / 1000.0
    for i in range(3):
        price = int(price + rndsell() * delta)
        quantity = int(random.random() * 100 + 10)
        r = client.post('/v1/contracts/orders', {
            'symbol': symbol,
            'type': 'LIMIT',
            'direction': 'SHORT',
            'leverage': 10,
            'price': price,
            'quantity': quantity
        })
        log('place SELL_LIMIT at %s ok? %s' % (price, error(r)))
        time.sleep(random.random() * SLEEP)

def rndbuy():
    return random.random() - 0.3

def rndsell():
    return random.random() - 0.8

# keep as (price, timestamp):
WORLD_PRICE = (None, 0)

HEADERS = {
    'User-Agent': 'Python3'
}

def get_world_price(remoteSymbol):
    '''
    get world price, cache at least 5 seconds.
    '''
    global WORLD_PRICE
    if WORLD_PRICE[1] > time.time():
        return WORLD_PRICE[0]
    # update price:
    url = 'https://api.pro.coinbase.com/products/%s/ticker' % remoteSymbol
    log('try get world price from %s...' % url)
    req = request.Request(url, headers=HEADERS)
    with request.urlopen(req, timeout=5) as f:
        s = f.read()
        r = json.loads(s.decode('utf-8'))
        log('got price: %s' % r['price'])
        WORLD_PRICE = (int(float(r['price'])), time.time())
    return WORLD_PRICE[0]

def log(s):
    now = datetime.datetime.now().strftime('%H:%m:%S')
    print('[%s] %s' % (now, s))

if __name__ == '__main__':
    main()
