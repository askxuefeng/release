#!/bin/bash

cd "$(dirname "$0")"

# kill java:
echo -e '\x1b[37;41m kill ContractsTradingApplication processes for crypto... \x1b[m'
ps aux | grep ContractsTradingApplication | awk '{print $2}' | xargs -I {} sh -c "echo 'kill -9 {}'"
ps aux | grep ContractsTradingApplication | awk '{print $2}' | xargs kill -9
ps aux | grep ContractsTradingApplication

echo -e '\x1b[37;41m done \x1b[m'
