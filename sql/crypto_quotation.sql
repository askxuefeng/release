

-- BEGIN generate DDL --

DROP DATABASE IF EXISTS crypto_quotation;

CREATE DATABASE crypto_quotation;

CREATE USER IF NOT EXISTS crypto_rw@'%' IDENTIFIED BY 'crypto_rw_password';

CREATE USER IF NOT EXISTS crypto_ro@'%' IDENTIFIED BY 'crypto_ro_password';

GRANT SELECT,INSERT,DELETE,UPDATE ON crypto_quotation.* TO crypto_rw@'%' WITH GRANT OPTION;

GRANT SELECT ON crypto_quotation.* TO crypto_ro@'%' WITH GRANT OPTION;

FLUSH PRIVILEGES;

USE crypto_quotation;

CREATE TABLE day_bars (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE hour_bars (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE min_bars (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE sec_bars (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE ticks (
  makerOrderId BIGINT NOT NULL,
  takerOrderId BIGINT NOT NULL,
  takerDirection BOOL NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  timestamp BIGINT NOT NULL,
  price DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  INDEX IDX_TS_SYMBOLID (timestamp,symbolId),
  PRIMARY KEY(makerOrderId, takerOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- day_bars_202003 --
CREATE TABLE day_bars_202003 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- day_bars_202004 --
CREATE TABLE day_bars_202004 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- day_bars_202005 --
CREATE TABLE day_bars_202005 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- day_bars_202006 --
CREATE TABLE day_bars_202006 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- day_bars_202007 --
CREATE TABLE day_bars_202007 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- day_bars_202008 --
CREATE TABLE day_bars_202008 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- day_bars_202009 --
CREATE TABLE day_bars_202009 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- day_bars_202010 --
CREATE TABLE day_bars_202010 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- day_bars_202011 --
CREATE TABLE day_bars_202011 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- day_bars_202012 --
CREATE TABLE day_bars_202012 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- day_bars_202101 --
CREATE TABLE day_bars_202101 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- day_bars_202102 --
CREATE TABLE day_bars_202102 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- hour_bars_202003 --
CREATE TABLE hour_bars_202003 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- hour_bars_202004 --
CREATE TABLE hour_bars_202004 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- hour_bars_202005 --
CREATE TABLE hour_bars_202005 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- hour_bars_202006 --
CREATE TABLE hour_bars_202006 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- hour_bars_202007 --
CREATE TABLE hour_bars_202007 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- hour_bars_202008 --
CREATE TABLE hour_bars_202008 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- hour_bars_202009 --
CREATE TABLE hour_bars_202009 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- hour_bars_202010 --
CREATE TABLE hour_bars_202010 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- hour_bars_202011 --
CREATE TABLE hour_bars_202011 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- hour_bars_202012 --
CREATE TABLE hour_bars_202012 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- hour_bars_202101 --
CREATE TABLE hour_bars_202101 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- hour_bars_202102 --
CREATE TABLE hour_bars_202102 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- min_bars_202003 --
CREATE TABLE min_bars_202003 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- min_bars_202004 --
CREATE TABLE min_bars_202004 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- min_bars_202005 --
CREATE TABLE min_bars_202005 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- min_bars_202006 --
CREATE TABLE min_bars_202006 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- min_bars_202007 --
CREATE TABLE min_bars_202007 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- min_bars_202008 --
CREATE TABLE min_bars_202008 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- min_bars_202009 --
CREATE TABLE min_bars_202009 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- min_bars_202010 --
CREATE TABLE min_bars_202010 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- min_bars_202011 --
CREATE TABLE min_bars_202011 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- min_bars_202012 --
CREATE TABLE min_bars_202012 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- min_bars_202101 --
CREATE TABLE min_bars_202101 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- min_bars_202102 --
CREATE TABLE min_bars_202102 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- sec_bars_202003 --
CREATE TABLE sec_bars_202003 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- sec_bars_202004 --
CREATE TABLE sec_bars_202004 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- sec_bars_202005 --
CREATE TABLE sec_bars_202005 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- sec_bars_202006 --
CREATE TABLE sec_bars_202006 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- sec_bars_202007 --
CREATE TABLE sec_bars_202007 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- sec_bars_202008 --
CREATE TABLE sec_bars_202008 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- sec_bars_202009 --
CREATE TABLE sec_bars_202009 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- sec_bars_202010 --
CREATE TABLE sec_bars_202010 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- sec_bars_202011 --
CREATE TABLE sec_bars_202011 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- sec_bars_202012 --
CREATE TABLE sec_bars_202012 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- sec_bars_202101 --
CREATE TABLE sec_bars_202101 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- sec_bars_202102 --
CREATE TABLE sec_bars_202102 (
  startTime BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  closePrice DOUBLE NOT NULL,
  highPrice DOUBLE NOT NULL,
  lowPrice DOUBLE NOT NULL,
  openPrice DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  PRIMARY KEY(startTime, symbolId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- ticks_202003 --
CREATE TABLE ticks_202003 (
  makerOrderId BIGINT NOT NULL,
  takerOrderId BIGINT NOT NULL,
  takerDirection BOOL NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  timestamp BIGINT NOT NULL,
  price DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  INDEX IDX_TS_SYMBOLID (timestamp,symbolId),
  PRIMARY KEY(makerOrderId, takerOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- ticks_202004 --
CREATE TABLE ticks_202004 (
  makerOrderId BIGINT NOT NULL,
  takerOrderId BIGINT NOT NULL,
  takerDirection BOOL NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  timestamp BIGINT NOT NULL,
  price DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  INDEX IDX_TS_SYMBOLID (timestamp,symbolId),
  PRIMARY KEY(makerOrderId, takerOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- ticks_202005 --
CREATE TABLE ticks_202005 (
  makerOrderId BIGINT NOT NULL,
  takerOrderId BIGINT NOT NULL,
  takerDirection BOOL NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  timestamp BIGINT NOT NULL,
  price DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  INDEX IDX_TS_SYMBOLID (timestamp,symbolId),
  PRIMARY KEY(makerOrderId, takerOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- ticks_202006 --
CREATE TABLE ticks_202006 (
  makerOrderId BIGINT NOT NULL,
  takerOrderId BIGINT NOT NULL,
  takerDirection BOOL NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  timestamp BIGINT NOT NULL,
  price DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  INDEX IDX_TS_SYMBOLID (timestamp,symbolId),
  PRIMARY KEY(makerOrderId, takerOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- ticks_202007 --
CREATE TABLE ticks_202007 (
  makerOrderId BIGINT NOT NULL,
  takerOrderId BIGINT NOT NULL,
  takerDirection BOOL NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  timestamp BIGINT NOT NULL,
  price DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  INDEX IDX_TS_SYMBOLID (timestamp,symbolId),
  PRIMARY KEY(makerOrderId, takerOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- ticks_202008 --
CREATE TABLE ticks_202008 (
  makerOrderId BIGINT NOT NULL,
  takerOrderId BIGINT NOT NULL,
  takerDirection BOOL NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  timestamp BIGINT NOT NULL,
  price DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  INDEX IDX_TS_SYMBOLID (timestamp,symbolId),
  PRIMARY KEY(makerOrderId, takerOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- ticks_202009 --
CREATE TABLE ticks_202009 (
  makerOrderId BIGINT NOT NULL,
  takerOrderId BIGINT NOT NULL,
  takerDirection BOOL NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  timestamp BIGINT NOT NULL,
  price DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  INDEX IDX_TS_SYMBOLID (timestamp,symbolId),
  PRIMARY KEY(makerOrderId, takerOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- ticks_202010 --
CREATE TABLE ticks_202010 (
  makerOrderId BIGINT NOT NULL,
  takerOrderId BIGINT NOT NULL,
  takerDirection BOOL NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  timestamp BIGINT NOT NULL,
  price DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  INDEX IDX_TS_SYMBOLID (timestamp,symbolId),
  PRIMARY KEY(makerOrderId, takerOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- ticks_202011 --
CREATE TABLE ticks_202011 (
  makerOrderId BIGINT NOT NULL,
  takerOrderId BIGINT NOT NULL,
  takerDirection BOOL NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  timestamp BIGINT NOT NULL,
  price DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  INDEX IDX_TS_SYMBOLID (timestamp,symbolId),
  PRIMARY KEY(makerOrderId, takerOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- ticks_202012 --
CREATE TABLE ticks_202012 (
  makerOrderId BIGINT NOT NULL,
  takerOrderId BIGINT NOT NULL,
  takerDirection BOOL NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  timestamp BIGINT NOT NULL,
  price DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  INDEX IDX_TS_SYMBOLID (timestamp,symbolId),
  PRIMARY KEY(makerOrderId, takerOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- ticks_202101 --
CREATE TABLE ticks_202101 (
  makerOrderId BIGINT NOT NULL,
  takerOrderId BIGINT NOT NULL,
  takerDirection BOOL NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  timestamp BIGINT NOT NULL,
  price DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  INDEX IDX_TS_SYMBOLID (timestamp,symbolId),
  PRIMARY KEY(makerOrderId, takerOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- ticks_202102 --
CREATE TABLE ticks_202102 (
  makerOrderId BIGINT NOT NULL,
  takerOrderId BIGINT NOT NULL,
  takerDirection BOOL NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  timestamp BIGINT NOT NULL,
  price DOUBLE NOT NULL,
  volume DOUBLE NOT NULL,
  INDEX IDX_TS_SYMBOLID (timestamp,symbolId),
  PRIMARY KEY(makerOrderId, takerOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- END generate DDL --
