

-- BEGIN generate DDL --

DROP DATABASE IF EXISTS crypto_contracts;

CREATE DATABASE crypto_contracts;

CREATE USER IF NOT EXISTS crypto_rw@'%' IDENTIFIED BY 'crypto_rw_password';

CREATE USER IF NOT EXISTS crypto_ro@'%' IDENTIFIED BY 'crypto_ro_password';

GRANT SELECT,INSERT,DELETE,UPDATE ON crypto_contracts.* TO crypto_rw@'%' WITH GRANT OPTION;

GRANT SELECT ON crypto_contracts.* TO crypto_ro@'%' WITH GRANT OPTION;

FLUSH PRIVILEGES;

USE crypto_contracts;

CREATE TABLE contracts_accounts (
  currencyId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  updatedAt BIGINT NOT NULL,
  available DECIMAL(36,18) NOT NULL,
  frozen DECIMAL(36,18) NOT NULL,
  position DECIMAL(36,18) NOT NULL,
  PRIMARY KEY(currencyId, userId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE contracts_api_post_requests (
  id BIGINT AUTO_INCREMENT NOT NULL,
  createdAt BIGINT NOT NULL,
  apiType VARCHAR(32) NOT NULL,
  apiPath VARCHAR(100) NOT NULL,
  apiBody VARCHAR(1000) NOT NULL,
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE contracts_events (
  sequenceId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  previousId BIGINT NOT NULL,
  data VARCHAR(10000) NOT NULL,
  CONSTRAINT UNI_PREV_ID UNIQUE (previousId),
  PRIMARY KEY(sequenceId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE contracts_id_generator (
  id BIGINT AUTO_INCREMENT NOT NULL,
  createdAt BIGINT NOT NULL,
  name VARCHAR(32) NOT NULL,
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE contracts_locks (
  id VARCHAR(32) NOT NULL,
  expiresAt BIGINT NOT NULL,
  owner VARCHAR(100) NOT NULL,
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE contracts_match_details (
  counterOrderId BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  feeCurrencyId BIGINT NOT NULL,
  orderCreatedAt BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  quantity DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_CT (createdAt),
  PRIMARY KEY(orderId, counterOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE contracts_orders (
  id BIGINT NOT NULL,
  features INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  marginCurrencyId BIGINT NOT NULL,
  quantity BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  unfilledQuantity BIGINT NOT NULL,
  updatedAt BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fillPrice DOUBLE NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  makerFeeRate DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  takerFeeRate DECIMAL(36,18) NOT NULL,
  trailingBasePrice DECIMAL(36,18) NOT NULL,
  trailingDistance DECIMAL(36,18) NOT NULL,
  triggerOn DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  status VARCHAR(50) NOT NULL,
  triggerDirection VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE contracts_position_clearings (
  id BIGINT AUTO_INCREMENT NOT NULL,
  counterOrderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  quantityAfterClearing BIGINT NOT NULL,
  quantityChanged BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  clearingPrice DECIMAL(36,18) NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  positionMargin DECIMAL(36,18) NOT NULL,
  rate DECIMAL(36,18) NOT NULL,
  realizedPNLChanged DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  CONSTRAINT UNI_SEQ_ORDER_USER_SYM_TYPE UNIQUE (sequenceId,orderId,counterOrderId,userId,symbolId,type),
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE contracts_settings (
  id VARCHAR(100) NOT NULL,
  updatedAt BIGINT NOT NULL,
  setting VARCHAR(100) NOT NULL,
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE contracts_transfer_ins (
  id BIGINT AUTO_INCREMENT NOT NULL,
  done BOOL NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  updatedAt BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  amount DECIMAL(36,18) NOT NULL,
  currencyName VARCHAR(32) NOT NULL,
  transferFrom VARCHAR(50) NOT NULL,
  transferId VARCHAR(32) NOT NULL UNIQUE,
  transferTo VARCHAR(50) NOT NULL,
  INDEX IDX_CAT_DONE (createdAt,done),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE contracts_transfer_logs (
  id BIGINT AUTO_INCREMENT NOT NULL,
  done BOOL NOT NULL,
  error BOOL NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  updatedAt BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  amount DECIMAL(36,18) NOT NULL,
  currencyName VARCHAR(32) NOT NULL,
  transferFrom VARCHAR(50) NOT NULL,
  transferId VARCHAR(32) NOT NULL UNIQUE,
  transferTo VARCHAR(50) NOT NULL,
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE contracts_transfer_outs (
  id BIGINT AUTO_INCREMENT NOT NULL,
  done BOOL NOT NULL,
  error BOOL NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  updatedAt BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  amount DECIMAL(36,18) NOT NULL,
  currencyName VARCHAR(32) NOT NULL,
  transferFrom VARCHAR(50) NOT NULL,
  transferId VARCHAR(32) NOT NULL UNIQUE,
  transferTo VARCHAR(50) NOT NULL,
  INDEX IDX_CAT_DONE (createdAt,done),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE contracts_unique_events (
  uniqueId VARCHAR(50) NOT NULL,
  createdAt BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  PRIMARY KEY(uniqueId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE contracts_unreplays (
  uniqueId VARCHAR(32) NOT NULL,
  userId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  INDEX IDX_CREATEDAT (createdAt),
  PRIMARY KEY(uniqueId, userId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE contracts_user_blacklists (
  id BIGINT AUTO_INCREMENT NOT NULL,
  expiresAt BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  type VARCHAR(50) NOT NULL,
  CONSTRAINT UNI_type_userId_symbolId UNIQUE (type, userId, symbolId),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE contracts_user_whitelists (
  id BIGINT AUTO_INCREMENT NOT NULL,
  expiresAt BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  type VARCHAR(50) NOT NULL,
  CONSTRAINT UNI_type_userId_symbolId UNIQUE (type, userId, symbolId),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_events_0 --
CREATE TABLE contracts_events_0 (
  sequenceId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  previousId BIGINT NOT NULL,
  data VARCHAR(10000) NOT NULL,
  CONSTRAINT UNI_PREV_ID UNIQUE (previousId),
  PRIMARY KEY(sequenceId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_events_1 --
CREATE TABLE contracts_events_1 (
  sequenceId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  previousId BIGINT NOT NULL,
  data VARCHAR(10000) NOT NULL,
  CONSTRAINT UNI_PREV_ID UNIQUE (previousId),
  PRIMARY KEY(sequenceId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_events_2 --
CREATE TABLE contracts_events_2 (
  sequenceId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  previousId BIGINT NOT NULL,
  data VARCHAR(10000) NOT NULL,
  CONSTRAINT UNI_PREV_ID UNIQUE (previousId),
  PRIMARY KEY(sequenceId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_events_3 --
CREATE TABLE contracts_events_3 (
  sequenceId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  previousId BIGINT NOT NULL,
  data VARCHAR(10000) NOT NULL,
  CONSTRAINT UNI_PREV_ID UNIQUE (previousId),
  PRIMARY KEY(sequenceId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_events_4 --
CREATE TABLE contracts_events_4 (
  sequenceId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  previousId BIGINT NOT NULL,
  data VARCHAR(10000) NOT NULL,
  CONSTRAINT UNI_PREV_ID UNIQUE (previousId),
  PRIMARY KEY(sequenceId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_events_5 --
CREATE TABLE contracts_events_5 (
  sequenceId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  previousId BIGINT NOT NULL,
  data VARCHAR(10000) NOT NULL,
  CONSTRAINT UNI_PREV_ID UNIQUE (previousId),
  PRIMARY KEY(sequenceId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_events_6 --
CREATE TABLE contracts_events_6 (
  sequenceId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  previousId BIGINT NOT NULL,
  data VARCHAR(10000) NOT NULL,
  CONSTRAINT UNI_PREV_ID UNIQUE (previousId),
  PRIMARY KEY(sequenceId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_events_7 --
CREATE TABLE contracts_events_7 (
  sequenceId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  previousId BIGINT NOT NULL,
  data VARCHAR(10000) NOT NULL,
  CONSTRAINT UNI_PREV_ID UNIQUE (previousId),
  PRIMARY KEY(sequenceId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_events_8 --
CREATE TABLE contracts_events_8 (
  sequenceId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  previousId BIGINT NOT NULL,
  data VARCHAR(10000) NOT NULL,
  CONSTRAINT UNI_PREV_ID UNIQUE (previousId),
  PRIMARY KEY(sequenceId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_events_9 --
CREATE TABLE contracts_events_9 (
  sequenceId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  previousId BIGINT NOT NULL,
  data VARCHAR(10000) NOT NULL,
  CONSTRAINT UNI_PREV_ID UNIQUE (previousId),
  PRIMARY KEY(sequenceId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_events_10 --
CREATE TABLE contracts_events_10 (
  sequenceId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  previousId BIGINT NOT NULL,
  data VARCHAR(10000) NOT NULL,
  CONSTRAINT UNI_PREV_ID UNIQUE (previousId),
  PRIMARY KEY(sequenceId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_events_11 --
CREATE TABLE contracts_events_11 (
  sequenceId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  previousId BIGINT NOT NULL,
  data VARCHAR(10000) NOT NULL,
  CONSTRAINT UNI_PREV_ID UNIQUE (previousId),
  PRIMARY KEY(sequenceId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_match_details_202003 --
CREATE TABLE contracts_match_details_202003 (
  counterOrderId BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  feeCurrencyId BIGINT NOT NULL,
  orderCreatedAt BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  quantity DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_CT (createdAt),
  PRIMARY KEY(orderId, counterOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_match_details_202004 --
CREATE TABLE contracts_match_details_202004 (
  counterOrderId BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  feeCurrencyId BIGINT NOT NULL,
  orderCreatedAt BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  quantity DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_CT (createdAt),
  PRIMARY KEY(orderId, counterOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_match_details_202005 --
CREATE TABLE contracts_match_details_202005 (
  counterOrderId BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  feeCurrencyId BIGINT NOT NULL,
  orderCreatedAt BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  quantity DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_CT (createdAt),
  PRIMARY KEY(orderId, counterOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_match_details_202006 --
CREATE TABLE contracts_match_details_202006 (
  counterOrderId BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  feeCurrencyId BIGINT NOT NULL,
  orderCreatedAt BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  quantity DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_CT (createdAt),
  PRIMARY KEY(orderId, counterOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_match_details_202007 --
CREATE TABLE contracts_match_details_202007 (
  counterOrderId BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  feeCurrencyId BIGINT NOT NULL,
  orderCreatedAt BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  quantity DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_CT (createdAt),
  PRIMARY KEY(orderId, counterOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_match_details_202008 --
CREATE TABLE contracts_match_details_202008 (
  counterOrderId BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  feeCurrencyId BIGINT NOT NULL,
  orderCreatedAt BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  quantity DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_CT (createdAt),
  PRIMARY KEY(orderId, counterOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_match_details_202009 --
CREATE TABLE contracts_match_details_202009 (
  counterOrderId BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  feeCurrencyId BIGINT NOT NULL,
  orderCreatedAt BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  quantity DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_CT (createdAt),
  PRIMARY KEY(orderId, counterOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_match_details_202010 --
CREATE TABLE contracts_match_details_202010 (
  counterOrderId BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  feeCurrencyId BIGINT NOT NULL,
  orderCreatedAt BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  quantity DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_CT (createdAt),
  PRIMARY KEY(orderId, counterOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_match_details_202011 --
CREATE TABLE contracts_match_details_202011 (
  counterOrderId BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  feeCurrencyId BIGINT NOT NULL,
  orderCreatedAt BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  quantity DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_CT (createdAt),
  PRIMARY KEY(orderId, counterOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_match_details_202012 --
CREATE TABLE contracts_match_details_202012 (
  counterOrderId BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  feeCurrencyId BIGINT NOT NULL,
  orderCreatedAt BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  quantity DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_CT (createdAt),
  PRIMARY KEY(orderId, counterOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_match_details_202101 --
CREATE TABLE contracts_match_details_202101 (
  counterOrderId BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  feeCurrencyId BIGINT NOT NULL,
  orderCreatedAt BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  quantity DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_CT (createdAt),
  PRIMARY KEY(orderId, counterOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_match_details_202102 --
CREATE TABLE contracts_match_details_202102 (
  counterOrderId BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  feeCurrencyId BIGINT NOT NULL,
  orderCreatedAt BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  quantity DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_CT (createdAt),
  PRIMARY KEY(orderId, counterOrderId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_orders_202003 --
CREATE TABLE contracts_orders_202003 (
  id BIGINT NOT NULL,
  features INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  marginCurrencyId BIGINT NOT NULL,
  quantity BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  unfilledQuantity BIGINT NOT NULL,
  updatedAt BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fillPrice DOUBLE NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  makerFeeRate DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  takerFeeRate DECIMAL(36,18) NOT NULL,
  trailingBasePrice DECIMAL(36,18) NOT NULL,
  trailingDistance DECIMAL(36,18) NOT NULL,
  triggerOn DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  status VARCHAR(50) NOT NULL,
  triggerDirection VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_orders_202004 --
CREATE TABLE contracts_orders_202004 (
  id BIGINT NOT NULL,
  features INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  marginCurrencyId BIGINT NOT NULL,
  quantity BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  unfilledQuantity BIGINT NOT NULL,
  updatedAt BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fillPrice DOUBLE NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  makerFeeRate DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  takerFeeRate DECIMAL(36,18) NOT NULL,
  trailingBasePrice DECIMAL(36,18) NOT NULL,
  trailingDistance DECIMAL(36,18) NOT NULL,
  triggerOn DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  status VARCHAR(50) NOT NULL,
  triggerDirection VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_orders_202005 --
CREATE TABLE contracts_orders_202005 (
  id BIGINT NOT NULL,
  features INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  marginCurrencyId BIGINT NOT NULL,
  quantity BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  unfilledQuantity BIGINT NOT NULL,
  updatedAt BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fillPrice DOUBLE NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  makerFeeRate DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  takerFeeRate DECIMAL(36,18) NOT NULL,
  trailingBasePrice DECIMAL(36,18) NOT NULL,
  trailingDistance DECIMAL(36,18) NOT NULL,
  triggerOn DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  status VARCHAR(50) NOT NULL,
  triggerDirection VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_orders_202006 --
CREATE TABLE contracts_orders_202006 (
  id BIGINT NOT NULL,
  features INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  marginCurrencyId BIGINT NOT NULL,
  quantity BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  unfilledQuantity BIGINT NOT NULL,
  updatedAt BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fillPrice DOUBLE NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  makerFeeRate DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  takerFeeRate DECIMAL(36,18) NOT NULL,
  trailingBasePrice DECIMAL(36,18) NOT NULL,
  trailingDistance DECIMAL(36,18) NOT NULL,
  triggerOn DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  status VARCHAR(50) NOT NULL,
  triggerDirection VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_orders_202007 --
CREATE TABLE contracts_orders_202007 (
  id BIGINT NOT NULL,
  features INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  marginCurrencyId BIGINT NOT NULL,
  quantity BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  unfilledQuantity BIGINT NOT NULL,
  updatedAt BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fillPrice DOUBLE NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  makerFeeRate DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  takerFeeRate DECIMAL(36,18) NOT NULL,
  trailingBasePrice DECIMAL(36,18) NOT NULL,
  trailingDistance DECIMAL(36,18) NOT NULL,
  triggerOn DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  status VARCHAR(50) NOT NULL,
  triggerDirection VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_orders_202008 --
CREATE TABLE contracts_orders_202008 (
  id BIGINT NOT NULL,
  features INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  marginCurrencyId BIGINT NOT NULL,
  quantity BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  unfilledQuantity BIGINT NOT NULL,
  updatedAt BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fillPrice DOUBLE NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  makerFeeRate DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  takerFeeRate DECIMAL(36,18) NOT NULL,
  trailingBasePrice DECIMAL(36,18) NOT NULL,
  trailingDistance DECIMAL(36,18) NOT NULL,
  triggerOn DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  status VARCHAR(50) NOT NULL,
  triggerDirection VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_orders_202009 --
CREATE TABLE contracts_orders_202009 (
  id BIGINT NOT NULL,
  features INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  marginCurrencyId BIGINT NOT NULL,
  quantity BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  unfilledQuantity BIGINT NOT NULL,
  updatedAt BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fillPrice DOUBLE NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  makerFeeRate DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  takerFeeRate DECIMAL(36,18) NOT NULL,
  trailingBasePrice DECIMAL(36,18) NOT NULL,
  trailingDistance DECIMAL(36,18) NOT NULL,
  triggerOn DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  status VARCHAR(50) NOT NULL,
  triggerDirection VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_orders_202010 --
CREATE TABLE contracts_orders_202010 (
  id BIGINT NOT NULL,
  features INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  marginCurrencyId BIGINT NOT NULL,
  quantity BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  unfilledQuantity BIGINT NOT NULL,
  updatedAt BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fillPrice DOUBLE NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  makerFeeRate DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  takerFeeRate DECIMAL(36,18) NOT NULL,
  trailingBasePrice DECIMAL(36,18) NOT NULL,
  trailingDistance DECIMAL(36,18) NOT NULL,
  triggerOn DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  status VARCHAR(50) NOT NULL,
  triggerDirection VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_orders_202011 --
CREATE TABLE contracts_orders_202011 (
  id BIGINT NOT NULL,
  features INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  marginCurrencyId BIGINT NOT NULL,
  quantity BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  unfilledQuantity BIGINT NOT NULL,
  updatedAt BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fillPrice DOUBLE NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  makerFeeRate DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  takerFeeRate DECIMAL(36,18) NOT NULL,
  trailingBasePrice DECIMAL(36,18) NOT NULL,
  trailingDistance DECIMAL(36,18) NOT NULL,
  triggerOn DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  status VARCHAR(50) NOT NULL,
  triggerDirection VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_orders_202012 --
CREATE TABLE contracts_orders_202012 (
  id BIGINT NOT NULL,
  features INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  marginCurrencyId BIGINT NOT NULL,
  quantity BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  unfilledQuantity BIGINT NOT NULL,
  updatedAt BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fillPrice DOUBLE NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  makerFeeRate DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  takerFeeRate DECIMAL(36,18) NOT NULL,
  trailingBasePrice DECIMAL(36,18) NOT NULL,
  trailingDistance DECIMAL(36,18) NOT NULL,
  triggerOn DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  status VARCHAR(50) NOT NULL,
  triggerDirection VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_orders_202101 --
CREATE TABLE contracts_orders_202101 (
  id BIGINT NOT NULL,
  features INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  marginCurrencyId BIGINT NOT NULL,
  quantity BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  unfilledQuantity BIGINT NOT NULL,
  updatedAt BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fillPrice DOUBLE NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  makerFeeRate DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  takerFeeRate DECIMAL(36,18) NOT NULL,
  trailingBasePrice DECIMAL(36,18) NOT NULL,
  trailingDistance DECIMAL(36,18) NOT NULL,
  triggerOn DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  status VARCHAR(50) NOT NULL,
  triggerDirection VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_orders_202102 --
CREATE TABLE contracts_orders_202102 (
  id BIGINT NOT NULL,
  features INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  marginCurrencyId BIGINT NOT NULL,
  quantity BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  unfilledQuantity BIGINT NOT NULL,
  updatedAt BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  fillPrice DOUBLE NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  makerFeeRate DECIMAL(36,18) NOT NULL,
  price DECIMAL(36,18) NOT NULL,
  takerFeeRate DECIMAL(36,18) NOT NULL,
  trailingBasePrice DECIMAL(36,18) NOT NULL,
  trailingDistance DECIMAL(36,18) NOT NULL,
  triggerOn DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  status VARCHAR(50) NOT NULL,
  triggerDirection VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_position_clearings_202003 --
CREATE TABLE contracts_position_clearings_202003 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  counterOrderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  quantityAfterClearing BIGINT NOT NULL,
  quantityChanged BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  clearingPrice DECIMAL(36,18) NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  positionMargin DECIMAL(36,18) NOT NULL,
  rate DECIMAL(36,18) NOT NULL,
  realizedPNLChanged DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  CONSTRAINT UNI_SEQ_ORDER_USER_SYM_TYPE UNIQUE (sequenceId,orderId,counterOrderId,userId,symbolId,type),
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_position_clearings_202004 --
CREATE TABLE contracts_position_clearings_202004 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  counterOrderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  quantityAfterClearing BIGINT NOT NULL,
  quantityChanged BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  clearingPrice DECIMAL(36,18) NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  positionMargin DECIMAL(36,18) NOT NULL,
  rate DECIMAL(36,18) NOT NULL,
  realizedPNLChanged DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  CONSTRAINT UNI_SEQ_ORDER_USER_SYM_TYPE UNIQUE (sequenceId,orderId,counterOrderId,userId,symbolId,type),
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_position_clearings_202005 --
CREATE TABLE contracts_position_clearings_202005 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  counterOrderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  quantityAfterClearing BIGINT NOT NULL,
  quantityChanged BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  clearingPrice DECIMAL(36,18) NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  positionMargin DECIMAL(36,18) NOT NULL,
  rate DECIMAL(36,18) NOT NULL,
  realizedPNLChanged DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  CONSTRAINT UNI_SEQ_ORDER_USER_SYM_TYPE UNIQUE (sequenceId,orderId,counterOrderId,userId,symbolId,type),
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_position_clearings_202006 --
CREATE TABLE contracts_position_clearings_202006 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  counterOrderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  quantityAfterClearing BIGINT NOT NULL,
  quantityChanged BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  clearingPrice DECIMAL(36,18) NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  positionMargin DECIMAL(36,18) NOT NULL,
  rate DECIMAL(36,18) NOT NULL,
  realizedPNLChanged DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  CONSTRAINT UNI_SEQ_ORDER_USER_SYM_TYPE UNIQUE (sequenceId,orderId,counterOrderId,userId,symbolId,type),
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_position_clearings_202007 --
CREATE TABLE contracts_position_clearings_202007 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  counterOrderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  quantityAfterClearing BIGINT NOT NULL,
  quantityChanged BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  clearingPrice DECIMAL(36,18) NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  positionMargin DECIMAL(36,18) NOT NULL,
  rate DECIMAL(36,18) NOT NULL,
  realizedPNLChanged DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  CONSTRAINT UNI_SEQ_ORDER_USER_SYM_TYPE UNIQUE (sequenceId,orderId,counterOrderId,userId,symbolId,type),
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_position_clearings_202008 --
CREATE TABLE contracts_position_clearings_202008 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  counterOrderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  quantityAfterClearing BIGINT NOT NULL,
  quantityChanged BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  clearingPrice DECIMAL(36,18) NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  positionMargin DECIMAL(36,18) NOT NULL,
  rate DECIMAL(36,18) NOT NULL,
  realizedPNLChanged DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  CONSTRAINT UNI_SEQ_ORDER_USER_SYM_TYPE UNIQUE (sequenceId,orderId,counterOrderId,userId,symbolId,type),
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_position_clearings_202009 --
CREATE TABLE contracts_position_clearings_202009 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  counterOrderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  quantityAfterClearing BIGINT NOT NULL,
  quantityChanged BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  clearingPrice DECIMAL(36,18) NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  positionMargin DECIMAL(36,18) NOT NULL,
  rate DECIMAL(36,18) NOT NULL,
  realizedPNLChanged DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  CONSTRAINT UNI_SEQ_ORDER_USER_SYM_TYPE UNIQUE (sequenceId,orderId,counterOrderId,userId,symbolId,type),
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_position_clearings_202010 --
CREATE TABLE contracts_position_clearings_202010 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  counterOrderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  quantityAfterClearing BIGINT NOT NULL,
  quantityChanged BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  clearingPrice DECIMAL(36,18) NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  positionMargin DECIMAL(36,18) NOT NULL,
  rate DECIMAL(36,18) NOT NULL,
  realizedPNLChanged DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  CONSTRAINT UNI_SEQ_ORDER_USER_SYM_TYPE UNIQUE (sequenceId,orderId,counterOrderId,userId,symbolId,type),
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_position_clearings_202011 --
CREATE TABLE contracts_position_clearings_202011 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  counterOrderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  quantityAfterClearing BIGINT NOT NULL,
  quantityChanged BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  clearingPrice DECIMAL(36,18) NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  positionMargin DECIMAL(36,18) NOT NULL,
  rate DECIMAL(36,18) NOT NULL,
  realizedPNLChanged DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  CONSTRAINT UNI_SEQ_ORDER_USER_SYM_TYPE UNIQUE (sequenceId,orderId,counterOrderId,userId,symbolId,type),
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_position_clearings_202012 --
CREATE TABLE contracts_position_clearings_202012 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  counterOrderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  quantityAfterClearing BIGINT NOT NULL,
  quantityChanged BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  clearingPrice DECIMAL(36,18) NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  positionMargin DECIMAL(36,18) NOT NULL,
  rate DECIMAL(36,18) NOT NULL,
  realizedPNLChanged DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  CONSTRAINT UNI_SEQ_ORDER_USER_SYM_TYPE UNIQUE (sequenceId,orderId,counterOrderId,userId,symbolId,type),
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_position_clearings_202101 --
CREATE TABLE contracts_position_clearings_202101 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  counterOrderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  quantityAfterClearing BIGINT NOT NULL,
  quantityChanged BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  clearingPrice DECIMAL(36,18) NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  positionMargin DECIMAL(36,18) NOT NULL,
  rate DECIMAL(36,18) NOT NULL,
  realizedPNLChanged DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  CONSTRAINT UNI_SEQ_ORDER_USER_SYM_TYPE UNIQUE (sequenceId,orderId,counterOrderId,userId,symbolId,type),
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- contracts_position_clearings_202102 --
CREATE TABLE contracts_position_clearings_202102 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  counterOrderId BIGINT NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  orderId BIGINT NOT NULL,
  quantityAfterClearing BIGINT NOT NULL,
  quantityChanged BIGINT NOT NULL,
  sequenceId BIGINT NOT NULL,
  symbolId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  clearingPrice DECIMAL(36,18) NOT NULL,
  fee DECIMAL(36,18) NOT NULL,
  positionMargin DECIMAL(36,18) NOT NULL,
  rate DECIMAL(36,18) NOT NULL,
  realizedPNLChanged DECIMAL(36,18) NOT NULL,
  direction VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  CONSTRAINT UNI_SEQ_ORDER_USER_SYM_TYPE UNIQUE (sequenceId,orderId,counterOrderId,userId,symbolId,type),
  INDEX IDX_USERID_ID (userId,id),
  INDEX IDX_USERID_SYMBOLID_ID (userId,symbolId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- END generate DDL --
