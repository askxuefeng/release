

-- BEGIN generate DDL --

DROP DATABASE IF EXISTS crypto_wallet;

CREATE DATABASE crypto_wallet;

CREATE USER IF NOT EXISTS crypto_rw@'%' IDENTIFIED BY 'crypto_rw_password';

CREATE USER IF NOT EXISTS crypto_ro@'%' IDENTIFIED BY 'crypto_ro_password';

GRANT SELECT,INSERT,DELETE,UPDATE ON crypto_wallet.* TO crypto_rw@'%' WITH GRANT OPTION;

GRANT SELECT ON crypto_wallet.* TO crypto_ro@'%' WITH GRANT OPTION;

FLUSH PRIVILEGES;

USE crypto_wallet;

CREATE TABLE financial_interest_payments (
  id BIGINT AUTO_INCREMENT NOT NULL,
  interestInterval INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  interestStartTime BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  financialBalance DECIMAL(36,18) NOT NULL,
  interestAmount DECIMAL(36,18) NOT NULL,
  interestRate DECIMAL(36,18) NOT NULL,
  interestTimeUnit VARCHAR(50) NOT NULL,
  CONSTRAINT UNI_USER_CUR_START UNIQUE (userId, currencyId, interestStartTime),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE financial_withdraw_requests (
  currencyId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  amount DECIMAL(10,0) NOT NULL,
  CONSTRAINT UNI_USER_CUR_TYPE UNIQUE (userId, currencyId),
  PRIMARY KEY(currencyId, userId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE offline_watch_addresses (
  id BIGINT AUTO_INCREMENT NOT NULL,
  blockHeight BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  updatedAt BIGINT NOT NULL,
  version BIGINT NOT NULL,
  balance DECIMAL(36,18) NOT NULL,
  currency VARCHAR(32) NOT NULL,
  address VARCHAR(100) NOT NULL,
  blockHash VARCHAR(100) NOT NULL,
  description VARCHAR(1000) NOT NULL,
  CONSTRAINT UNI_CUR_ADDR UNIQUE (currency, address),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE wallet_account_flows (
  id BIGINT AUTO_INCREMENT NOT NULL,
  sign BOOL NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  amount DECIMAL(36,18) NOT NULL,
  flowType VARCHAR(50) NOT NULL,
  transferType VARCHAR(50) NOT NULL,
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  INDEX IDX_USER_ID (userId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE wallet_accounts (
  id BIGINT AUTO_INCREMENT NOT NULL,
  currencyId BIGINT NOT NULL,
  updatedAt BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  balance DECIMAL(36,18) NOT NULL,
  type VARCHAR(50) NOT NULL,
  CONSTRAINT UNI_USER_CUR_TYPE UNIQUE (userId, currencyId, type),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE wallet_deposit_addresses (
  id BIGINT AUTO_INCREMENT NOT NULL,
  bipIndex INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  CONSTRAINT UNI_BIP_CUR UNIQUE (bipIndex, currencyId),
  CONSTRAINT UNI_USERID_CUR UNIQUE (userId, currencyId),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE wallet_deposit_results (
  id BIGINT AUTO_INCREMENT NOT NULL,
  shouldAudit BOOL NOT NULL,
  bipIndex INTEGER NOT NULL,
  confirms INTEGER NOT NULL,
  minimumConfirms INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  updatedAt BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  version BIGINT NOT NULL,
  amount DECIMAL(36,18) NOT NULL,
  currency VARCHAR(32) NOT NULL,
  status VARCHAR(50) NOT NULL,
  uniqueId VARCHAR(200) NOT NULL,
  encryptedToAddress VARCHAR(1000) NOT NULL,
  CONSTRAINT UNI_UNIQUE_ID UNIQUE (uniqueId),
  INDEX IDX_USER_ID (userId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE wallet_financial_clearings (
  id BIGINT AUTO_INCREMENT NOT NULL,
  interestInterval INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  interestStartTime BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  financialBalance DECIMAL(36,18) NOT NULL,
  interestAmount DECIMAL(36,18) NOT NULL,
  interestRate DECIMAL(36,18) NOT NULL,
  interestTimeUnit VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USER_ID (userId,id),
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE wallet_locks (
  id VARCHAR(32) NOT NULL,
  expiresAt BIGINT NOT NULL,
  owner VARCHAR(100) NOT NULL,
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE wallet_transfer_ins (
  id BIGINT AUTO_INCREMENT NOT NULL,
  done BOOL NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  updatedAt BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  amount DECIMAL(36,18) NOT NULL,
  currencyName VARCHAR(32) NOT NULL,
  transferFrom VARCHAR(50) NOT NULL,
  transferId VARCHAR(32) NOT NULL UNIQUE,
  transferTo VARCHAR(50) NOT NULL,
  INDEX IDX_CREATEDAT (createdAt),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE wallet_transfer_logs (
  id BIGINT AUTO_INCREMENT NOT NULL,
  done BOOL NOT NULL,
  error BOOL NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  updatedAt BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  amount DECIMAL(36,18) NOT NULL,
  currencyName VARCHAR(32) NOT NULL,
  transferFrom VARCHAR(50) NOT NULL,
  transferId VARCHAR(32) NOT NULL UNIQUE,
  transferTo VARCHAR(50) NOT NULL,
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE wallet_transfer_outs (
  id BIGINT AUTO_INCREMENT NOT NULL,
  done BOOL NOT NULL,
  error BOOL NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  updatedAt BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  amount DECIMAL(36,18) NOT NULL,
  currencyName VARCHAR(32) NOT NULL,
  transferFrom VARCHAR(50) NOT NULL,
  transferId VARCHAR(32) NOT NULL UNIQUE,
  transferTo VARCHAR(50) NOT NULL,
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE wallet_unreplays (
  uniqueId VARCHAR(32) NOT NULL,
  userId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  INDEX IDX_CREATEDAT (createdAt),
  PRIMARY KEY(uniqueId, userId)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE wallet_withdraw_addresses (
  id BIGINT AUTO_INCREMENT NOT NULL,
  createdAt BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  addressCurrency VARCHAR(32) NOT NULL,
  addressHash VARCHAR(100) NOT NULL,
  description VARCHAR(100) NOT NULL,
  encryptedAddress VARCHAR(1000) NOT NULL,
  CONSTRAINT UNI_USER_CUR_ADDRHASH UNIQUE (userId, addressCurrency, addressHash),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;


CREATE TABLE wallet_withdraw_requests (
  id BIGINT NOT NULL,
  needAudit BOOL NOT NULL,
  confirmExpiresAt BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  updatedAt BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  version BIGINT NOT NULL,
  withdrawTxId BIGINT NOT NULL,
  amount DECIMAL(36,18) NOT NULL,
  withdrawFee DECIMAL(36,18) NOT NULL,
  challenge VARCHAR(32) NOT NULL,
  currency VARCHAR(32) NOT NULL,
  errorCode VARCHAR(32) NOT NULL,
  status VARCHAR(50) NOT NULL,
  encryptedToAddress VARCHAR(200) NOT NULL,
  errorMessage VARCHAR(100) NOT NULL,
  tx VARCHAR(100) NOT NULL,
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- wallet_account_flows_202003 --
CREATE TABLE wallet_account_flows_202003 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  sign BOOL NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  amount DECIMAL(36,18) NOT NULL,
  flowType VARCHAR(50) NOT NULL,
  transferType VARCHAR(50) NOT NULL,
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  INDEX IDX_USER_ID (userId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- wallet_account_flows_202004 --
CREATE TABLE wallet_account_flows_202004 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  sign BOOL NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  amount DECIMAL(36,18) NOT NULL,
  flowType VARCHAR(50) NOT NULL,
  transferType VARCHAR(50) NOT NULL,
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  INDEX IDX_USER_ID (userId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- wallet_account_flows_202005 --
CREATE TABLE wallet_account_flows_202005 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  sign BOOL NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  amount DECIMAL(36,18) NOT NULL,
  flowType VARCHAR(50) NOT NULL,
  transferType VARCHAR(50) NOT NULL,
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  INDEX IDX_USER_ID (userId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- wallet_account_flows_202006 --
CREATE TABLE wallet_account_flows_202006 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  sign BOOL NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  amount DECIMAL(36,18) NOT NULL,
  flowType VARCHAR(50) NOT NULL,
  transferType VARCHAR(50) NOT NULL,
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  INDEX IDX_USER_ID (userId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- wallet_account_flows_202007 --
CREATE TABLE wallet_account_flows_202007 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  sign BOOL NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  amount DECIMAL(36,18) NOT NULL,
  flowType VARCHAR(50) NOT NULL,
  transferType VARCHAR(50) NOT NULL,
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  INDEX IDX_USER_ID (userId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- wallet_account_flows_202008 --
CREATE TABLE wallet_account_flows_202008 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  sign BOOL NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  amount DECIMAL(36,18) NOT NULL,
  flowType VARCHAR(50) NOT NULL,
  transferType VARCHAR(50) NOT NULL,
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  INDEX IDX_USER_ID (userId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- wallet_account_flows_202009 --
CREATE TABLE wallet_account_flows_202009 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  sign BOOL NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  amount DECIMAL(36,18) NOT NULL,
  flowType VARCHAR(50) NOT NULL,
  transferType VARCHAR(50) NOT NULL,
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  INDEX IDX_USER_ID (userId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- wallet_account_flows_202010 --
CREATE TABLE wallet_account_flows_202010 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  sign BOOL NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  amount DECIMAL(36,18) NOT NULL,
  flowType VARCHAR(50) NOT NULL,
  transferType VARCHAR(50) NOT NULL,
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  INDEX IDX_USER_ID (userId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- wallet_account_flows_202011 --
CREATE TABLE wallet_account_flows_202011 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  sign BOOL NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  amount DECIMAL(36,18) NOT NULL,
  flowType VARCHAR(50) NOT NULL,
  transferType VARCHAR(50) NOT NULL,
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  INDEX IDX_USER_ID (userId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- wallet_account_flows_202012 --
CREATE TABLE wallet_account_flows_202012 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  sign BOOL NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  amount DECIMAL(36,18) NOT NULL,
  flowType VARCHAR(50) NOT NULL,
  transferType VARCHAR(50) NOT NULL,
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  INDEX IDX_USER_ID (userId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- wallet_account_flows_202101 --
CREATE TABLE wallet_account_flows_202101 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  sign BOOL NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  amount DECIMAL(36,18) NOT NULL,
  flowType VARCHAR(50) NOT NULL,
  transferType VARCHAR(50) NOT NULL,
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  INDEX IDX_USER_ID (userId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- wallet_account_flows_202102 --
CREATE TABLE wallet_account_flows_202102 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  sign BOOL NOT NULL,
  counterUserId BIGINT NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  amount DECIMAL(36,18) NOT NULL,
  flowType VARCHAR(50) NOT NULL,
  transferType VARCHAR(50) NOT NULL,
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  INDEX IDX_USER_ID (userId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- wallet_financial_clearings_202003 --
CREATE TABLE wallet_financial_clearings_202003 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  interestInterval INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  interestStartTime BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  financialBalance DECIMAL(36,18) NOT NULL,
  interestAmount DECIMAL(36,18) NOT NULL,
  interestRate DECIMAL(36,18) NOT NULL,
  interestTimeUnit VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USER_ID (userId,id),
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- wallet_financial_clearings_202004 --
CREATE TABLE wallet_financial_clearings_202004 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  interestInterval INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  interestStartTime BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  financialBalance DECIMAL(36,18) NOT NULL,
  interestAmount DECIMAL(36,18) NOT NULL,
  interestRate DECIMAL(36,18) NOT NULL,
  interestTimeUnit VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USER_ID (userId,id),
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- wallet_financial_clearings_202005 --
CREATE TABLE wallet_financial_clearings_202005 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  interestInterval INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  interestStartTime BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  financialBalance DECIMAL(36,18) NOT NULL,
  interestAmount DECIMAL(36,18) NOT NULL,
  interestRate DECIMAL(36,18) NOT NULL,
  interestTimeUnit VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USER_ID (userId,id),
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- wallet_financial_clearings_202006 --
CREATE TABLE wallet_financial_clearings_202006 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  interestInterval INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  interestStartTime BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  financialBalance DECIMAL(36,18) NOT NULL,
  interestAmount DECIMAL(36,18) NOT NULL,
  interestRate DECIMAL(36,18) NOT NULL,
  interestTimeUnit VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USER_ID (userId,id),
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- wallet_financial_clearings_202007 --
CREATE TABLE wallet_financial_clearings_202007 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  interestInterval INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  interestStartTime BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  financialBalance DECIMAL(36,18) NOT NULL,
  interestAmount DECIMAL(36,18) NOT NULL,
  interestRate DECIMAL(36,18) NOT NULL,
  interestTimeUnit VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USER_ID (userId,id),
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- wallet_financial_clearings_202008 --
CREATE TABLE wallet_financial_clearings_202008 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  interestInterval INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  interestStartTime BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  financialBalance DECIMAL(36,18) NOT NULL,
  interestAmount DECIMAL(36,18) NOT NULL,
  interestRate DECIMAL(36,18) NOT NULL,
  interestTimeUnit VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USER_ID (userId,id),
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- wallet_financial_clearings_202009 --
CREATE TABLE wallet_financial_clearings_202009 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  interestInterval INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  interestStartTime BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  financialBalance DECIMAL(36,18) NOT NULL,
  interestAmount DECIMAL(36,18) NOT NULL,
  interestRate DECIMAL(36,18) NOT NULL,
  interestTimeUnit VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USER_ID (userId,id),
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- wallet_financial_clearings_202010 --
CREATE TABLE wallet_financial_clearings_202010 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  interestInterval INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  interestStartTime BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  financialBalance DECIMAL(36,18) NOT NULL,
  interestAmount DECIMAL(36,18) NOT NULL,
  interestRate DECIMAL(36,18) NOT NULL,
  interestTimeUnit VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USER_ID (userId,id),
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- wallet_financial_clearings_202011 --
CREATE TABLE wallet_financial_clearings_202011 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  interestInterval INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  interestStartTime BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  financialBalance DECIMAL(36,18) NOT NULL,
  interestAmount DECIMAL(36,18) NOT NULL,
  interestRate DECIMAL(36,18) NOT NULL,
  interestTimeUnit VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USER_ID (userId,id),
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- wallet_financial_clearings_202012 --
CREATE TABLE wallet_financial_clearings_202012 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  interestInterval INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  interestStartTime BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  financialBalance DECIMAL(36,18) NOT NULL,
  interestAmount DECIMAL(36,18) NOT NULL,
  interestRate DECIMAL(36,18) NOT NULL,
  interestTimeUnit VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USER_ID (userId,id),
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- wallet_financial_clearings_202101 --
CREATE TABLE wallet_financial_clearings_202101 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  interestInterval INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  interestStartTime BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  financialBalance DECIMAL(36,18) NOT NULL,
  interestAmount DECIMAL(36,18) NOT NULL,
  interestRate DECIMAL(36,18) NOT NULL,
  interestTimeUnit VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USER_ID (userId,id),
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- wallet_financial_clearings_202102 --
CREATE TABLE wallet_financial_clearings_202102 (
  id BIGINT AUTO_INCREMENT NOT NULL,
  interestInterval INTEGER NOT NULL,
  createdAt BIGINT NOT NULL,
  currencyId BIGINT NOT NULL,
  interestStartTime BIGINT NOT NULL,
  userId BIGINT NOT NULL,
  financialBalance DECIMAL(36,18) NOT NULL,
  interestAmount DECIMAL(36,18) NOT NULL,
  interestRate DECIMAL(36,18) NOT NULL,
  interestTimeUnit VARCHAR(50) NOT NULL,
  type VARCHAR(50) NOT NULL,
  INDEX IDX_USER_ID (userId,id),
  INDEX IDX_USER_CURRENCY_ID (userId,currencyId,id),
  PRIMARY KEY(id)
) Engine=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=UTF8;

-- END generate DDL --
