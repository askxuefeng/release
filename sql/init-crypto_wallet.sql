

-- BEGIN generate INSERTS --

USE crypto_wallet;

INSERT INTO `wallet_accounts` (`userId`, `currencyId`, `type`, `balance`, `updatedAt`) VALUES (10000, 100, 'AVAILABLE', 9700, 1546963321192);
INSERT INTO `wallet_accounts` (`userId`, `currencyId`, `type`, `balance`, `updatedAt`) VALUES (10001, 100, 'AVAILABLE', 9700, 1546963321192);
INSERT INTO `wallet_accounts` (`userId`, `currencyId`, `type`, `balance`, `updatedAt`) VALUES (10002, 100, 'AVAILABLE', 9700, 1546963321192);
INSERT INTO `wallet_accounts` (`userId`, `currencyId`, `type`, `balance`, `updatedAt`) VALUES (10003, 100, 'AVAILABLE', 9700, 1546963321192);
INSERT INTO `wallet_accounts` (`userId`, `currencyId`, `type`, `balance`, `updatedAt`) VALUES (10004, 100, 'AVAILABLE', 9700, 1546963321192);
INSERT INTO `wallet_accounts` (`userId`, `currencyId`, `type`, `balance`, `updatedAt`) VALUES (10005, 100, 'AVAILABLE', 9700, 1546963321192);
INSERT INTO `wallet_accounts` (`userId`, `currencyId`, `type`, `balance`, `updatedAt`) VALUES (10006, 100, 'AVAILABLE', 9700, 1546963321192);
INSERT INTO `wallet_accounts` (`userId`, `currencyId`, `type`, `balance`, `updatedAt`) VALUES (10007, 100, 'AVAILABLE', 9700, 1546963321192);
INSERT INTO `wallet_accounts` (`userId`, `currencyId`, `type`, `balance`, `updatedAt`) VALUES (10008, 100, 'AVAILABLE', 9700, 1546963321192);
INSERT INTO `wallet_accounts` (`userId`, `currencyId`, `type`, `balance`, `updatedAt`) VALUES (10009, 100, 'AVAILABLE', 9700, 1546963321192);
INSERT INTO `wallet_accounts` (`userId`, `currencyId`, `type`, `balance`, `updatedAt`) VALUES (108, 100, 'AVAILABLE', -97000, 1546963321192);
INSERT INTO `wallet_accounts` (`userId`, `currencyId`, `type`, `balance`, `updatedAt`) VALUES (10000, 105, 'AVAILABLE', 359900, 1546963321192);
INSERT INTO `wallet_accounts` (`userId`, `currencyId`, `type`, `balance`, `updatedAt`) VALUES (10001, 105, 'AVAILABLE', 359900, 1546963321192);
INSERT INTO `wallet_accounts` (`userId`, `currencyId`, `type`, `balance`, `updatedAt`) VALUES (10002, 105, 'AVAILABLE', 359900, 1546963321192);
INSERT INTO `wallet_accounts` (`userId`, `currencyId`, `type`, `balance`, `updatedAt`) VALUES (10003, 105, 'AVAILABLE', 359900, 1546963321192);
INSERT INTO `wallet_accounts` (`userId`, `currencyId`, `type`, `balance`, `updatedAt`) VALUES (10004, 105, 'AVAILABLE', 359900, 1546963321192);
INSERT INTO `wallet_accounts` (`userId`, `currencyId`, `type`, `balance`, `updatedAt`) VALUES (10005, 105, 'AVAILABLE', 359900, 1546963321192);
INSERT INTO `wallet_accounts` (`userId`, `currencyId`, `type`, `balance`, `updatedAt`) VALUES (10006, 105, 'AVAILABLE', 359900, 1546963321192);
INSERT INTO `wallet_accounts` (`userId`, `currencyId`, `type`, `balance`, `updatedAt`) VALUES (10007, 105, 'AVAILABLE', 359900, 1546963321192);
INSERT INTO `wallet_accounts` (`userId`, `currencyId`, `type`, `balance`, `updatedAt`) VALUES (10008, 105, 'AVAILABLE', 359900, 1546963321192);
INSERT INTO `wallet_accounts` (`userId`, `currencyId`, `type`, `balance`, `updatedAt`) VALUES (10009, 105, 'AVAILABLE', 359900, 1546963321192);
INSERT INTO `wallet_accounts` (`userId`, `currencyId`, `type`, `balance`, `updatedAt`) VALUES (108, 105, 'AVAILABLE', -3599000, 1546963321192);


-- END generate INSERTS --

