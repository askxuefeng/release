#!/usr/bin/env python3

import os, json, argparse, datetime, subprocess
from os.path import expanduser

from support import *

def main():
    parser = argparse.ArgumentParser(description='package to AWS deployable')
    parser.add_argument('--profile', help='specify profile name')
    parser.add_argument('--tag', default='', help='specify tag as build number')
    args = parser.parse_args()
    profile = args.profile
    tag = args.tag
    # try read ../build.version:
    if os.path.isfile(basedir + '/build.tag'):
        tag = read_text(basedir + '/build.tag').strip()
        info('loaded version from build.tag: ' + tag)
    if not profile or not tag:
        parser.print_help()
        exit(1)

    snapshotdir = 'crypto-build/release/snapshot'
    configdir   = 'crypto-build/release/snapshot/config-repo'
    templdir    = 'crypto-build/release/template/java'

    info('set snapshotdir=%s' % snapshotdir)
    info('set configdir=%s' % configdir)
    info('set templdir=%s' % templdir)

    envfile = '%s/crypto-build/release/env/%s.env' % (basedir, profile)
    if not os.path.isfile(envfile):
        error('env file not found: %s' % envfile)
        exit(1)
    envdict = load_env(envfile)
    envdict['PROFILE'] = profile
    domain = envdict.get('DOMAIN', '')
    if not domain:
        error('env DOMAIN not found in file: %s' % envfile)
        exit(1)
    info('set env DOMAIN=%s' % domain)
    internalelb = envdict.get('INTERNAL_LOAD_BALANCER', '')
    if not internalelb:
        error('env INTERNAL_LOAD_BALANCER not found in file: %s, nginx config is invalid!' % envfile)

    run('mkdir -p %s' % profile, cwd='crypto-build/release/snapshot')

    for app in [f[7:-4] for f in os.listdir('%s/%s/target' % (basedir, snapshotdir)) if f.startswith('crypto-') and f.endswith('.jar')]:
        if fileExist('crypto-build/release/snapshot/target/crypto-%s.jar' % app):
            info('packaging app %s...' % app)
            appdir = '%s/%s/%s' % (snapshotdir, profile, app)
            for subdir in ('config', 'hooks', 'log'):
                run('mkdir -p %s/%s' % (appdir, subdir))
            run('cp %s/target/crypto-%s.jar %s' % (snapshotdir, app, appdir))
            run('cp %s/config-repo/application.yml %s/config/application.yml' % (snapshotdir, appdir))
            run('cp %s/config-repo/%s.yml %s/config/application-%s.yml' % (snapshotdir, app, appdir, profile))
            run('cp crypto-build/release/env/%s.env %s/config/%s.env' % (profile, appdir, profile))
            # copy template:
            vars = {
                'APP_NAME': app,
                'APP_FILE': 'crypto-' + app + '.jar',
                'DOMAIN': domain,
                'PROFILE': profile,
                'TAG': tag
            }
            copy_templates(templdir, vars, appdir)
            run('rm -rf %s-%s.tar.gz' % (app, tag), cwd='%s/crypto-build/release/snapshot/%s' % (basedir, profile))
            run('tar zcf %s-%s.tar.gz %s' % (app, tag, app), cwd='crypto-build/release/snapshot/' + profile)

    # generate nginx conf:
    app = App('www', 'nginx-proxy')
    appdir = '%s/%s/%s' % (snapshotdir, profile, app.name)
    info('packaging app %s...' % app.name)
    for subdir in ('config', 'hooks', 'root'):
        run('mkdir -p %s/%s' % (appdir, subdir))
    templdir = 'crypto-build/release/template/nginx'
    copy_templates(templdir, envdict, appdir)
    run('tar zxf %s/target/resources.tar.gz -C %s/root' % (snapshotdir, appdir))
    run('rm -rf %s-%s.tar.gz' % (app.name, tag), cwd='%s/crypto-build/release/snapshot/%s' % (basedir, profile))
    run('tar zcf %s-%s.tar.gz %s' % (app.name, tag, app.name), cwd='crypto-build/release/snapshot/' + profile)

    # generate yml zip:
    run('tar zcf configs.tar.gz config-repo', cwd=snapshotdir)

    generate_file('%s/crypto-build/release/snapshot/%s/unzip-all.sh' % (basedir, profile),
        '#!/bin/bash',
        'cd "$(dirname "$0")"',
        'find . -name "*.tar.gz" | sort | xargs -I {} sh -c "tar zxf {}" '
    )

    generate_file('%s/crypto-build/release/snapshot/%s/upload-all.sh' % (basedir, profile),
        '#!/bin/bash',
        'cd "$(dirname "$0")"',
        'scp *.tar.gz ubuntu@%s.interplanetbit.com:/home/ubuntu/deploy/' % profile
    )

    generate_file('%s/crypto-build/release/snapshot/%s/start-all.sh' % (basedir, profile),
        '#!/bin/bash',
        'cd "$(dirname "$0")"',
        ''' ls -l | grep "^d" | awk '{print $9}' | xargs -I {} sh "{}/QuickStart.sh" '''
    )

def generate_file(filePath, *lines):
    with open(filePath, 'w') as f:
        f.write('\n'.join(lines))

def load_env(envfile):
    d = {}
    with open(envfile, 'r') as f:
        for line in f:
            l = line.strip()
            if l and not l.startswith('#'):
                ss = l.split('=', 1)
                k = ss[0].strip()
                v = ss[1].strip()
                d[k] = v
    return d

def copy_templates(templdir, vars, appdir):
    templroot = os.path.join(basedir, templdir)
    for fd in os.listdir(templroot):
        if os.path.isfile(os.path.join(templroot, fd)):
            copy_template(templdir, fd, vars, appdir)
        if os.path.isdir(os.path.join(templroot, fd)):
            for f in os.listdir(os.path.join(templroot, fd)):
                if os.path.isfile(os.path.join(templroot, fd, f)):
                    copy_template(os.path.join(templdir, fd), f, vars, os.path.join(appdir, fd))

def copy_template(sourcedir, file, vars, targetdir):
    if is_text_file(file):
        info('from template "%s/%s" generating "%s"...' % (sourcedir, file, targetdir))
        s = read_text(os.path.join(basedir, sourcedir, file))
        for k, v in vars.items():
            s = s.replace('${%s}' % k, v)
        write_text(os.path.join(basedir, targetdir, file), s);
    else:
        run('cp %s/%s %s/' % (sourcedir, file, targetdir))

def is_text_file(file):
    for ext in ('.sh', '.json', '.conf', '.yml'):
        if file.endswith(ext):
            return True
    return False

def read_text(file):
    with open(file, 'rt', encoding='utf-8') as f:
        return f.read()

def write_text(file, s):
    with open(file, 'wt', encoding='utf-8') as f:
        f.write(s)

if __name__ == '__main__':
    main()
