#!/usr/bin/env bash

export APP_PATH=/home/ubuntu/deploy/${APP_NAME}
export ENV_FILE=$APP_PATH/config/${PROFILE}.env
export $(grep -v '^#' $ENV_FILE | xargs)

# java env:
# not export APP_PORT=8080
export SPRING_PROFILES_ACTIVE=${PROFILE}

# log and dd:
export LOG_PATH=$APP_PATH/log
export LOG_FILE=$LOG_PATH/${APP_NAME}.log
export JSON_LOG_FILE=$LOG_PATH/${APP_NAME}.json.log

cd $APP_PATH

PID=`ps aux | grep '[\/]${APP_NAME}' | awk '{print $2}'`

if [ -n "$PID" ]; then
    echo "kill exist ${APP_NAME} process by pid $PID..."
    kill -9 $PID
fi

nohup java -server -Xmx2048M -jar $APP_PATH/${APP_FILE} > /dev/null 2>&1 &
