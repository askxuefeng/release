#!/usr/bin/env bash

export APP_PATH=/home/ubuntu/deploy/${APP_NAME}
export ENV_FILE=$APP_PATH/config/${PROFILE}.env
export $(grep -v '^#' $ENV_FILE | xargs)

# java env:
export SPRING_PROFILES_ACTIVE=${PROFILE}

if [ $SPRING_PROFILES_ACTIVE = "native" ]; then
  echo "using default port for native profile."
else
  echo "using port 8080 for $SPRING_PROFILES_ACTIVE profile."
  export APP_PORT=8080
fi

# log:
export LOG_PATH=$APP_PATH/log
export LOG_FILE=$LOG_PATH/${APP_NAME}.log
export JSON_LOG_FILE=$LOG_PATH/${APP_NAME}.json.log

# dd-java-agent: https://docs.datadoghq.com/tracing/setup/java/
export DD_SERVICE_NAME=${APP_NAME}
export DD_AGENT_PATH=$APP_PATH/config/dd-java-agent.jar
export DD_JMXFETCH_ENABLED=true

cd $APP_PATH

if [ -z "$JAVA_XMS" ]; then
    export JAVA_XMS=`cat /proc/meminfo | grep MemTotal | awk '{ print int($2 * 2 / 4000) }'`m
fi

if [ -z "$JAVA_XMX" ]; then
    export JAVA_XMX=`cat /proc/meminfo | grep MemTotal | awk '{ print int($2 * 3 / 4000) }'`m
fi

echo "using -Xms$JAVA_XMS -Xmx$JAVA_XMX as java opts."

nohup java -javaagent:$DD_AGENT_PATH -server -Xms$JAVA_XMS -Xmx$JAVA_XMX $JAVA_OPTS -jar $APP_PATH/${APP_FILE} > /dev/null 2>&1 &
