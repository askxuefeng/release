#!/usr/bin/env bash

set -e

app_path=/home/ubuntu/deploy/${APP_NAME}
source $app_path/config/${PROFILE}.env

# setup datadog log configuration

mkdir -p /etc/datadog-agent/conf.d/java.d
cp /home/ubuntu/deploy/${APP_NAME}/config/log.datadog.conf /etc/datadog-agent/conf.d/java.d/conf.yaml
chown -R dd-agent:dd-agent /etc/datadog-agent/conf.d/java.d/

sed -i -- "s/# log_format_json: false/log_format_json: true/g" /etc/datadog-agent/datadog.yaml
sed -i -- "s/# logs_enabled: false/logs_enabled: true/g" /etc/datadog-agent/datadog.yaml

# setup datadog apm configuration
# apm is enabled to true by default

# restart datadog
service datadog-agent restart

cd $app_path

chown -R ubuntu:ubuntu /home/ubuntu/deploy
