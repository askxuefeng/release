#!/usr/bin/env bash

export APP_PATH=/home/ubuntu/deploy/${APP_NAME}
source $APP_PATH/${PROFILE}.env

echo "Application Stopping"

PROCESS_ID=$(ps -ef | grep "$APP_PATH/${APP_FILE}" | grep -v grep | awk '{print $2}')

kill -s SIGTERM $PROCESS_ID

for i in {1..20}
do
  echo "Application stop $i times"
  PROCESS_ID=$(ps -ef | grep "$APP_PATH/${APP_FILE}" | grep -v grep | awk '{print $2}')
  if [ $i -eq 20 ]; then
    echo "Wait more than 10 seconds, kill it directly!"
    kill -s KILL $PROCESS_ID
    break
  fi
  if [ -z "$PROCESS_ID" ]; then
    echo "Application Stopped"
    break
  else
    echo "Stopping round $i, wait 1 more second."
    sleep 1
  fi
done
