#      _   _____________   ___  __    __________  _   ________
#     / | / / ____/  _/ | / / |/ /   / ____/ __ \/ | / / ____/
#    /  |/ / / __ / //  |/ /|   /   / /   / / / /  |/ / /_
#   / /|  / /_/ // // /|  //   |   / /___/ /_/ / /|  / __/
#  /_/ |_/\____/___/_/ |_//_/|_|   \____/\____/_/ |_/_/
#

server {

    listen      80;
    listen [::]:80;

    server_name www.${DOMAIN};

    root /home/ubuntu/deploy/nginx-proxy/root;

    access_log /var/log/nginx/www_access_log;
    error_log  /var/log/nginx/www_error_log;

    location = /health {
        add_header Content-Type application/json;
        return 200 '{"status":"UP"}';
    }

    location ~ /static/ {
    }

    location / {
        # proxy to ui:8000
        proxy_pass       http://${INTERNAL_LOAD_BALANCER}:8000;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
}

server {

    listen      80;
    listen [::]:80;

	server_name manage.${DOMAIN};

    root       /var/www/html;

    access_log /var/log/nginx/manage_access_log;
    error_log  /var/log/nginx/manage_error_log;

    location = /health {
        add_header Content-Type application/json;
        return 200 '{"status":"UP"}';
    }

    location / {
        # proxy to manage:8008
        proxy_pass       http://${INTERNAL_LOAD_BALANCER}:8008;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
}

server {

    listen      80;
    listen [::]:80;

	server_name static.${DOMAIN};

    root /home/ubuntu/deploy/nginx-proxy/root;

    access_log /var/log/nginx/static_access_log;
    error_log  /var/log/nginx/static_error_log;

    location = /health {
        add_header Content-Type application/json;
        return 200 '{"status":"UP"}';
    }

    location / {
        if ($request_method = 'OPTIONS') {
            add_header 'Access-Control-Allow-Origin' '*';
            add_header 'Access-Control-Allow-Credentials' 'true';
            add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
            add_header 'Access-Control-Allow-Headers' 'DNT,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type';
            add_header 'Access-Control-Max-Age' 1728000;
            add_header 'Content-Type' 'text/plain charset=UTF-8';
            add_header 'Content-Length' 0;
            return 204;
        }
        if ($request_method = 'POST') {
            add_header 'Access-Control-Allow-Origin' '*';
            add_header 'Access-Control-Allow-Credentials' 'true';
            add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
            add_header 'Access-Control-Allow-Headers' 'DNT,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type';
        }
        if ($request_method = 'GET') {
            add_header 'Access-Control-Allow-Origin' '*';
            add_header 'Access-Control-Allow-Credentials' 'true';
            add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
            add_header 'Access-Control-Allow-Headers' 'DNT,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type';
        }
    }
}

server {

    listen      80;
    listen [::]:80;

	server_name api.${DOMAIN};

    root       /var/www/html;

    access_log /var/log/nginx/api_access_log;
    error_log  /var/log/nginx/api_error_log;

    location = /health {
        add_header Content-Type application/json;
        return 200 '{"status":"UP"}';
    }

    location = /v1/market/statistics {
        proxy_pass       http://${INTERNAL_LOAD_BALANCER}:8201;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location ~ /v1/contracts/ {
        proxy_pass       http://${INTERNAL_LOAD_BALANCER}:8201;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location ~ /v1/market/ {
        proxy_pass       http://${INTERNAL_LOAD_BALANCER}:8001;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location ~ /v1/users/ {
        proxy_pass       http://${INTERNAL_LOAD_BALANCER}:8001;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location ~ /v1/spots/ {
        proxy_pass       http://${INTERNAL_LOAD_BALANCER}:8101;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location ~ /v1/margin/ {
        proxy_pass       http://${INTERNAL_LOAD_BALANCER}:8101;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location ~ /v1/wallet/ {
        proxy_pass       http://${INTERNAL_LOAD_BALANCER}:8901;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location ~ /v1/financial/ {
        proxy_pass       http://${INTERNAL_LOAD_BALANCER}:8901;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location ~ /v1/indexes/ {
        proxy_pass       http://${INTERNAL_LOAD_BALANCER}:8201;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
}

server {

    listen      80;
    listen [::]:80;

	server_name wss.${DOMAIN};

    root       /var/www/html;

    access_log /var/log/nginx/wss_access_log;
    error_log  /var/log/nginx/wss_error_log;

    location = /health {
        add_header Content-Type application/json;
        return 200 '{"status":"UP"}';
    }

    location / {
        proxy_pass       http://${INTERNAL_LOAD_BALANCER}:8003;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
}
