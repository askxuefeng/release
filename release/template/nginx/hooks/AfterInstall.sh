#!/usr/bin/env bash

set -e

app_path=/home/ubuntu/deploy/nginx-proxy

# copy nginx configuration
cp /home/ubuntu/deploy/nginx-proxy/config/default.conf /etc/nginx/sites-available/default

chown root:root /etc/nginx/sites-available/default
