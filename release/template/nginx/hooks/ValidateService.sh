#!/usr/bin/env bash

SERVICE_CHECK_URL=http://localhost:8080/health

while true
do
  STATUS=$(curl -s -o /dev/null -w '%{http_code}' $SERVICE_CHECK_URL)
  if [ $STATUS -eq 200 ]; then
    echo "Got 200! App is alive!"
    break
  else
    echo "Got $STATUS, wait 10 seconds to retry $SERVICE_CHECK_URL ..."
    sleep 10
  fi
done
