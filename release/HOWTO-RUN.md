# HOW TO INSTALL INTERPLANETBIT EXCHANGE SYSTEM ON A SINGLE BOX

This document describes how to install Interplanetbit Exchange System on a single box.

## Prepare x86-64 Virtual Machine

Any kind of x86-64 VM (e.g. AWS EC2), at least 8 core CPU, 32G memory, 40G SSD.

## Prepare Ubuntu Server

Install Ubuntu Server 18.04 x64. e.g. Use AWS AMI: Ubuntu Server 18.04 LTS (HVM), SSD, 64 bit (x86).

Sudo user: `ubuntu`.

## Install MySQL 5.7

```
$ sudo apt install mysql-server
```

Check version:

```
$ mysql --version
mysql  Ver 14.14 Distrib 5.7.29, ...
```

Set `root` password to `password`:

```
$ mysql -u debian-sys-maint -p
Enter password: ******** (password of debian-sys-maint can be get from sudo cat /etc/mysql/debian.cnf)
mysql> UPDATE mysql.user SET authentication_string=PASSWORD('password'), plugin='mysql_native_password' where user='root';
Query OK, 1 row affected, 1 warning (0.00 sec)
Rows matched: 1  Changed: 1  Warnings: 1
mysql> exit
```

Login `root` with `password`:

```
$ sudo service mysql restart
$ mysql -u root -p
Enter password: ********
```

## Install Redis 5.x

```
$ sudo add-apt-repository -y ppa:chris-lea/redis-server
$ sudo apt-get update
$ sudo apt-get install redis-server
```

Check redis version:

```
$ redis-cli -v
redis-cli 5.0.8
```

## Install OpenJDK 11

```
$ sudo apt install openjdk-11-jdk-headless
```

Check java version:

```
$ java -version
openjdk version "11.0.6" ...
```
